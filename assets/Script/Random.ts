const {ccclass, property} = cc._decorator;

@ccclass
export default class Random extends cc.Component {

    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}

    //start () {}

    // update (dt) {}

    public static Range = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
}
