const {ccclass, property} = cc._decorator;

@ccclass
export default class TeamFeedBack extends cc.Component {

    @property(cc.Float)
    durantion = 0.0;

    @property(cc.Float)
    scale = 0.0;

    @property(cc.Float)
    final_scale = 0.0;

    ini_Scale = 0;

    action;

    // LIFE-CYCLE CALLBACKS:

    ClickEnter(){
        if(this.action!= null){
            this.node.stopAction(this.action);
        }

        this.action = cc.scaleTo(this.durantion, this.ini_Scale * this.scale);
        this.node.runAction(this.action);
    }

    ClickExit(){
        if(this.action!= null){
            this.node.stopAction(this.action);
        }

        this.action = cc.scaleTo(this.durantion, this.ini_Scale);
        this.node.runAction(this.action);
    }

    ClickDown(){
        if(this.action!= null){
            this.node.stopAction(this.action);
        }

        this.action = cc.scaleTo(this.durantion, this.ini_Scale * this.final_scale);
        this.node.runAction(this.action);
    }

    onLoad () {
        this.ini_Scale = this.node.scale;

        this.node.on(cc.Node.EventType.MOUSE_ENTER,this.ClickEnter,this);
        this.node.on(cc.Node.EventType.MOUSE_LEAVE,this.ClickExit,this);
        this.node.on(cc.Node.EventType.MOUSE_DOWN,this.ClickDown,this);
    }

    //start () {}

    // update (dt) {}

}
