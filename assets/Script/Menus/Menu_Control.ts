import ChangeScreen from "./ChangeScreen";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Menu_Control extends cc.Component {

    button: cc.Button = null;

    source: cc.AudioSource = null;

    @property(cc.AudioClip)
    Title: cc.AudioClip = null;

    @property(cc.AudioClip)
    PlayGame: cc.AudioClip = null;

    @property(cc.AudioClip)
    Intro: cc.AudioClip = null;

    sound_Atual: cc.AudioClip = null;

    @property(ChangeScreen)
    screens: ChangeScreen = null;

    @property(cc.Node)
    IndioIdle: cc.Node = null;

    @property(cc.Node)
    IndioSpeak: cc.Node = null;

    @property(cc.Node)
    skip: cc.Node = null;

    @property({
        type: cc.Float
    })
    timePlay = 0.0;

    @property({
        type: cc.Float
    })
    timeIntroduction = 0.0;

    clicked = false;

    ClickDown(){
        if(!this.clicked){
            this.sound_Atual = this.PlayGame;
            this.PlaySound();
    
            this.schedule(function() {
                this.Introduction();
                this.button.transition = cc.Button.Transition.NONE;
            }, this.timePlay,0);

            this.clicked = true;
        }
    }

    Introduction(){
        this.skip.active = true;

        this.sound_Atual = this.Intro;
        this.PlaySound();

        this.schedule(function() {
            this.screens.Start_ChooseMode();
        }, this.timeIntroduction + 0.25);

        this.IndioIdle.active = false;
        this.IndioSpeak.active = true;
    }

    Skip(){
        this.unscheduleAllCallbacks();

        this.screens.Start_ChooseMode();

        this.button.transition = cc.Button.Transition.NONE;
    }

    onLoad () {
        this.clicked = false;

        this.source = this.node.getComponent(cc.AudioSource);
        this.button = this.node.getComponent(cc.Button);

        this.skip.active = false;
        
        this.sound_Atual = this.Title;
        this.PlaySound();

        this.IndioIdle.active = true;
        this.IndioSpeak.active = false;

        this.node.on(cc.Node.EventType.MOUSE_DOWN,this.ClickDown,this);
    }

    PlaySound(){
        this.source.clip = this.sound_Atual;
        this.source.play();
    }

    //start () {}

    // update (dt) {}
}
