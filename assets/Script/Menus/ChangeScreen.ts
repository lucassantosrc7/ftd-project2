import Player from "../Indios/Player";

const {ccclass, property} = cc._decorator;

@ccclass
export default class ChangeScreen extends cc.Component {

    onLoad () {
        
    }

    Start_ChooseMode(){
        cc.director.loadScene("ChooseMode");
        cc.director.preloadScene("Game");
    }

    Start_Individual_Mode(){
        Player.Num_Players = 1;
        Player.final_Distance = 0;
        cc.director.loadScene("Game");
    }

    Start_Team_Mode(){
        cc.director.loadScene("TeamMode");
    }

    Start_Next_Race(){
        Player.final_Distance = 0;
        cc.director.loadScene("Game");
    }

    Two_Team_Mode(){
        Player.Num_Players = 2;
        Player.final_Distance = 0;
        cc.director.loadScene("Game");
    }

    Three_Team_Mode(){
        Player.Num_Players = 3;
        Player.final_Distance = 0;
        cc.director.loadScene("Game");
    }

    Four_Team_Mode(){
        Player.Num_Players = 4;
        Player.final_Distance = 0;
        cc.director.loadScene("Game");
    }

}
