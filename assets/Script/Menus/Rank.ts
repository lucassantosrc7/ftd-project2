import Opponent from "../Indios/Opponent";
import Player from "../Indios/Player";
import Random from "../Random";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Rank extends cc.Component {

    @property(cc.Label)
    message_Text: cc.Label = null;

    @property(cc.Label)
    player_Distance: cc.Label = null;

    @property
    player_String: string = '';

    @property(cc.Label)
    opponent_Distance: cc.Label = null;

    @property
    opponent_String: string = '';

    @property([cc.Node])
    last_Button: cc.Node = null;

    @property([cc.Node])
    Next_Button: cc.Node = null;

    static ModoTeam = false;

    static numVitoria = 0;

    source: cc.AudioSource = null;

    @property(cc.AudioClip)
    Result: cc.AudioClip = null;

    onLoad () {
        this.source = this.node.getComponent(cc.AudioSource);
        this.source.clip = this.Result;
        this.source.play();

        if(Opponent.final_Distance < 0){
            this.raffle_Opponent_Distance();
        }

        if(Opponent.final_Distance > Player.final_Distance){
            this.Print_Message("VOCÊ PERDEU!!!");
            Rank.numVitoria --;
        }else {
            this.Print_Message("VOCÊ GANHOU!!!");
            Rank.numVitoria ++;
        }

        if(Player.Num_Players <= 0){
            this.last_Button.active = true;
            this.Next_Button.active = false;

            if(Rank.ModoTeam){
                if(Rank.numVitoria > 0){
                    this.Print_Message("SEU TIME GANHOU!!!");
                }else{
                    this.Print_Message("SEU TIME PERDEU!!!");
                }
                
                Rank.ModoTeam = false;
            }
        }
        else
        {
            this.last_Button.active = false;
            this.Next_Button.active = true;

            Rank.ModoTeam = true;
        }
    }

    raffle_Opponent_Distance(){
        var num = Random.Range(0,101);

        if(num <= 20){
            Opponent.final_Distance = Player.final_Distance + Random.Range(21,26);
        }
        else if(num > 21 && num <= 50){
            Opponent.final_Distance = Player.final_Distance + Random.Range(11,21);
        }
        else if(num > 51){
            Opponent.final_Distance = Player.final_Distance + Random.Range(1,11);
        }
    }

    Print_Message(who_Win){
        this.player_Distance.string   = this.player_String + Player.final_Distance.toString() + " m";
        this.opponent_Distance.string = this.opponent_String + Opponent.final_Distance.toString() + " m";
        this.message_Text.string = who_Win;
    }

    //start () {}

    //update (dt) {}
}
