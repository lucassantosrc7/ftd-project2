import Countdown from "../Game/Countdown";
import CountDistance from "../Game/CountDistance";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Decrease_Speed extends cc.Component {

    @property({
        type: Number
    })
    Max_Time = 0;

    @property({
        type: Number
    })
    Min_Time = 0;

    @property({
        type: Number
    })
    Max_Distance = 0;

    @property(cc.Node)
    player: cc.Node = null;

    public static time_Actually = 1;
    
    callback: Function;

    onLoad () {
        Decrease_Speed.time_Actually = this.Max_Time;
    }

    //start () {}

    update (dt) {
        if(Countdown.Start){
            if(this.callback == null){
                this.Decrease();
            }

            Decrease_Speed.time_Actually = this.Max_Time - ((this.Min_Time * CountDistance.finaldistance)/ this.Max_Distance);
        }
    }

    public Decrease = function(){
        this.player.emit("Decrease_Speed");

        if(this.callback != null){
             this.unschedule(this.callback);
        }

        this.callback = function() {
            this.Decrease();
        };

        this.schedule(this.callback, Decrease_Speed.time_Actually);
    }
}
