import IndioAnim from "./IndioAnim";
import CountDistance from "../Game/CountDistance";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Player extends cc.Component {

    public static indioAnim: IndioAnim = null;

    public static Num_Players = 2;
    public static final_Distance = 0;

    Finish_Run = function(){
        this.PlayerNum();
        cc.director.loadScene("Rank");
    }

    PlayerNum = function() {
        Player.Num_Players--;
        Player.final_Distance = CountDistance.finaldistance;
    }

    onLoad () {
        this.node.on('Finish_Run', this.Finish_Run, this);
    }

    start () {
        Player.indioAnim = this.node.getComponent(IndioAnim);
    }
}
