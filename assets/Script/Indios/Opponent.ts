import IndioAnim from "./IndioAnim";
import Countdown from "../Game/Countdown";
import Player from "./Player";
import Random from "../Random";
import CountDistance from "../Game/CountDistance";

const {ccclass, property} = cc._decorator;

@ccclass
export default class Opponent extends cc.Component {
    opponent: IndioAnim = null;

    @property({
        type: Number
    })
    Max_Pos = 0.0; //position when the diffrence betwenten player.anim.timeScale and opponent.anim.timeScale is 1
    ini_Pos = 0;
    moveToX = 0;

    interval = 0;

    callback: Function;

    @property({
        type: Number
    })
    Min_Time_Prob = 0.0;
    @property({
        type: Number
    })
    Max_Time_Prob = 0.0;

    @property(cc.Float)
    Max_hit_Prob = 0.0;

    @property(cc.Float)
    Min_hit_Prob = 0.0;

    @property(cc.Float)
    Max_Distance = 0.0;

    @property(cc.Vec2)
    finaPos: cc.Vec2 = null;

    action: cc.Action = null;
    is_Moving = false;
    
    finish_Run = false;
    public static final_Distance = -1;

    onLoad () {
        this.node.on('Finish_Run',  this.Finish_Run, this);

        this.opponent = this.node.getComponent(IndioAnim);
        this.ini_Pos = this.node.position.x;

        Opponent.final_Distance = -1;
    }

    //start () {}

    update (dt) {
        if(Countdown.Start){
            if(this.callback == null){
                this.Input();
            }

            var _x = this.ini_Pos + ((this.Max_Pos * (Player.indioAnim.anim.timeScale - this.opponent.anim.timeScale))/0.75);

            if(!this.finish_Run && ((_x != this.node.position.x && !this.is_Moving) || _x != this.moveToX)){
                this.is_Moving = true;
                this.moveToX = _x;

                if(this.action != null){
                    this.node.stopAction(this.action);
                }
        
                this.action = cc.moveTo(1, new cc.Vec2(_x, this.node.position.y));
                this.node.runAction(this.action);
            }else if (this.finish_Run && this.finaPos.x != this.moveToX){
                this.moveToX = this.finaPos.x;

                if(this.action != null){
                    this.node.stopAction(this.action);
                }

                this.action = cc.moveTo(1, this.finaPos);
                this.node.runAction(this.action);
            }
            else if( _x == this.node.position.x){
                this.is_Moving = false;
            }
        }
     }

    Input (){
        if(this.callback != null){
            this.unschedule(this.callback);
        }

        if(!this.finish_Run){
            
            if(Random.Range(0, 101) < this.Relative_Prob(this.Min_hit_Prob,this.Max_hit_Prob)){
                this.node.emit("Increase_Speed");
            }else {
                this.node.emit("Decrease_Speed");
            }

            this.callback = function() {
                this.Input();
            };
            this.interval = Random.Range(this.Min_Time_Prob, this.Max_Time_Prob);
    
            this.schedule(this.callback, this.interval);
        }
    }

    Relative_Prob(min,max){
        if(CountDistance.finaldistance >= this.Max_Distance){
            return min;
        }

        return (max - (((max - min) * CountDistance.finaldistance)/this.Max_Distance));
    }

    Finish_Run(){
        this.finish_Run = true;
        Opponent.final_Distance = CountDistance.finaldistance;

        if(this.callback != null){
            this.unschedule(this.callback);
        }
    }
}
