import Countdown from "../Game/Countdown";

const {ccclass, property} = cc._decorator;

@ccclass
export default class IndioAnim extends cc.Component {

    anim: dragonBones.ArmatureDisplay = null;
    
    @property({
        type: cc.Float
    })
    change_Speed = 0.1;

    @property({
        type: cc.Float
    })
    min_Vel = 0.25;

    @property({
        type: cc.Float
    })
    max_Vel = 1.75;

    ini_Scale = 0;

    @property(cc.String)
    name_Anim = '';

    Increase_Speed = function(){
        if(this.anim.timeScale < this.max_Vel){
            this.anim.timeScale += this.change_Speed;
        }
        else if(this.anim.timeScale >= this.max_Vel){
            this.anim.timeScale = this.max_Vel;
        }
    }

    Decrease_Speed = function(){
        if(this.anim.timeScale > this.min_Vel) {
            this.anim.timeScale -= this.change_Speed;
        }
        else if(this.anim.timeScale <= this.min_Vel && Countdown.Start){
            this.anim.timeScale = 0;

            this.node.emit("Finish_Run");
        }
    }

    onLoad () {
        this.node.on('Increase_Speed', this.Increase_Speed, this);
        this.node.on('Decrease_Speed', this.Decrease_Speed, this);

        this.anim = this.node.getComponent(dragonBones.ArmatureDisplay);
    }

    update (dt) {
        if(Countdown.Start && this.anim.animationName != this.name_Anim){
            this.anim.playAnimation(this.name_Anim, -1);
        }
    }
}
