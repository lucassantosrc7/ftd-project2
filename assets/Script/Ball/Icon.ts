const {ccclass, property} = cc._decorator;

@ccclass
export default class Icon extends cc.Component {

    @property(Number)
    size_feedBack = 0.0;
    scale:cc.Vec2;

    @property(Number)
    rotate_feedBack = 0.0;

    action;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.scale = new cc.Vec2(this.node.scaleX,this.node.scaleY);
    }

    // update (dt) {}

    public Hit_FeedBack(){
        if(this.action != null){
            this.node.stopAction(this.action);
        }

        this.action = cc.sequence(
            cc.scaleTo(0.2, this.scale.x*this.size_feedBack, this.scale.y*this.size_feedBack),
            cc.scaleTo(0.2, this.scale.x, this.scale.y)
        );

        this.node.runAction(this.action);
    }

    public Missed_FeedBack(){
        if(this.action != null){
            this.node.stopAction(this.action);
        }

        this.action = cc.sequence(
            cc.rotateTo(0.2,this.rotate_feedBack),
            cc.rotateTo(0.2, 0),
            cc.rotateTo(0.2,this.rotate_feedBack),
            cc.rotateTo(0.2, 0)
        );

        this.node.runAction(this.action);
    }
}
