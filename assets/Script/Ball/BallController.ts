import Random from "../Random";
import Ball from "./Ball";
import CountDistance from "../Game/CountDistance";
import Countdown from "../Game/Countdown";

const {ccclass, property} = cc._decorator;

@ccclass
export default class BallController extends cc.Component {

    @property(cc.Node)
    icon: cc.Node = null;

    ball: Ball;
    public static current_Ball: Ball;

    @property(cc.Float)
    max_Max_TimeSpwan = 0.0;
    @property(cc.Float)
    min_Max_TimeSpwan = 0.0;

    @property(cc.Float)
    max_Min_TimeSpwan = 0.0;
    @property(cc.Float)
    min_Min_TimeSpwan = 0.0;

    @property(cc.Integer)
    Max_prob_BallGreen = 0;
    @property(cc.Integer)
    Min_prob_BallGreen = 0;

    @property(cc.Integer)
    Ini_BallPos = 0;

    @property(cc.Float)
    speed = 0.0;

    @property(cc.Float)
    Max_Speed = 0.0;

    @property(cc.Float)
    Max_Distance = 0.0;

    @property(cc.Float)
    destroyDistance = 0.0;

    @property(cc.Integer)
    check_BallPos = 0;

    @property(cc.Integer)
    uncheck_BallPos = 0;

    @property(cc.Float)
    pos_FullColor = 0.0;

    @property(cc.Prefab)
    greenBall: cc.Prefab = null;

    @property(cc.Prefab)
    yellowBall: cc.Prefab = null;

    started = false;

    // LIFE-CYCLE CALLBACKS:

    Spwan_Balls(){
        var percentage = Random.Range(0, 101);

        if(percentage <= this.Relative_Prob(this.Min_prob_BallGreen,this.Max_prob_BallGreen)){
            this.ball = cc.instantiate(this.greenBall).getComponent(Ball);
        }else{
            this.ball = cc.instantiate(this.yellowBall).getComponent(Ball);
        }

        this.ball.node.setParent(this.icon);
        this.ball.node.position = new cc.Vec2(this.Ini_BallPos, 0);

        var min = this.Relative_TimeSpawn(this.min_Min_TimeSpwan, this.max_Min_TimeSpwan);
        var max = this.Relative_TimeSpawn(this.min_Max_TimeSpwan, this.max_Max_TimeSpwan);

        var interval = Random.Range(min, max);

        this.schedule(function() {
            this.Spwan_Balls();
        }, interval, 0);
    }

    Relative_Prob(min,max){
        if(CountDistance.finaldistance >= this.Max_Distance){
            return min;
        }

        return (max - (((max - min) * CountDistance.finaldistance)/this.Max_Distance));
    }

    Relative_TimeSpawn(min,max){
        if(Ball.speed >= this.Max_Speed + this.speed){
            return max;
        }

        return (min - ((max * (Ball.speed - this.speed))/this.Max_Speed));
    }

    onLoad () {
        //Set ball properties
        this.max_Max_TimeSpwan = this.min_Max_TimeSpwan - this.max_Max_TimeSpwan;
        this.max_Min_TimeSpwan = this.min_Min_TimeSpwan - this.max_Min_TimeSpwan;

        this.Max_Speed -= this.speed;

        Ball.speed           = this.speed;
        Ball.destroyDistance = this.destroyDistance;
        Ball.check_BallPos   = this.check_BallPos;
        Ball.uncheck_BallPos = this.uncheck_BallPos;
        Ball.pos_FullColor   = this.pos_FullColor;
    }

    update(){
        if(Countdown.Start){
            if (!this.started){
                this.Spwan_Balls();

                this.started = true;
            }

            if(CountDistance.finaldistance < this.Max_Distance){
                Ball.speed = this.speed + ((this.Max_Speed * CountDistance.finaldistance)/ this.Max_Distance);
            }
        }            
    }
}
