import BallController from "./BallController";
import GameController from "../Game/GameController";

const {ccclass, property} = cc._decorator;

export enum Colors {
    Green,
    Yellow,
}

@ccclass
export default class Ball extends cc.Component {

    @property({
        type: cc.Enum(Colors)
    })
    public type_Ball: Colors = Colors.Green;

    public static speed = 0.0;
    public static destroyDistance = 0.0;
    public static uncheck_BallPos = 0.0;
    public static check_BallPos = 0.0;
    public static pos_FullColor = 0.0;

    public static game_Controller: GameController = null;

    cheking = false;
    pressed = false;
    ball_SCR: Ball = null;

    onLoad (){
        this.ball_SCR = this.node.getComponent(Ball);

        this.node.opacity = 0;
    }

    update (dt) {
        if(!this.pressed){
            if(this.node.position.x > Ball.pos_FullColor){
                this.node.opacity = 255 - (((Ball.pos_FullColor - this.node.position.x) * 255)/(Ball.pos_FullColor - 2700));
            }else if(this.node.opacity != 255){
                this.node.opacity = 255;
            }

            this.node.position = new cc.Vec2(this.node.position.x - Ball.speed * dt, this.node.position.y);

            if(this.node.position.x < Ball.destroyDistance){
                this.node.destroy();
            } else if(this.node.position.x < Ball.uncheck_BallPos && this.cheking){
                this.cheking = false;
                if(BallController.current_Ball != null){
                    BallController.current_Ball = null;
                }

                if(this.type_Ball == Colors.Green && Ball.game_Controller != null){
                    Ball.game_Controller.node.emit("Decrease_Speed");
                }
            } else if(this.node.position.x < Ball.check_BallPos && this.node.position.x > Ball.uncheck_BallPos){
                this.cheking = true;
                BallController.current_Ball = this.ball_SCR;
            }
        }
    }

    public FeedBack(){
        var scale = cc.scaleTo(0.2, 0.05, 0.05);
        var move = cc.moveTo(0.2,0,this.node.position.y);

        this.node.runAction(scale);
        this.node.runAction(move);

        this.cheking = false;

        this.schedule(function() {
            this.node.destroy();
        }, 0.25,0);

        this.pressed = true;
    }
}