import BallController from "../Ball/BallController";
import Ball, { Colors } from "../Ball/Ball";
import Countdown from "./Countdown";
import Icon from "../Ball/Icon";
import Tutorial_Controller from "./Tutorial_Controller";

const {ccclass, property} = cc._decorator;

@ccclass
export default class GameController extends cc.Component {

    @property(cc.Node)
    Canvas: cc.Node = null;

    @property(cc.Node)
    Indio: cc.Node = null;

    @property(Icon)
    icon: Icon = null;

    @property(cc.Node)
    stopWatch: cc.Node = null;

    static was_spoken = false;

    ClickDown(){
        if(Countdown.Start && BallController.current_Ball != null){
            var type_Ball = BallController.current_Ball.type_Ball;

            BallController.current_Ball.FeedBack();
            BallController.current_Ball = null;

            if(type_Ball == Colors.Green){
                this.Increase_Speed();
                this.icon.Hit_FeedBack();
                return;
            } else {
                this.Decrease_Speed();
                this.icon.Missed_FeedBack();
                return;
            }
        }
    }

    Decrease_Speed (){
        this.Indio.emit("Decrease_Speed");
    }

    Increase_Speed (){
        this.Indio.emit("Increase_Speed");
    }

    onLoad () {
        Countdown.Start = false;
        Ball.game_Controller = this.node.getComponent(GameController);

        // add key down and key up event
        this.Canvas.on(cc.Node.EventType.MOUSE_DOWN,this.ClickDown,this);
        this.node.on('Decrease_Speed', this.Decrease_Speed, this);
    }

    start(){
        if(!GameController.was_spoken){
            this.stopWatch.active = false;

            var tutorial = this.node.getComponent(Tutorial_Controller);
            tutorial.Start_Tutorial();

            GameController.was_spoken = true;
        }
    }
}