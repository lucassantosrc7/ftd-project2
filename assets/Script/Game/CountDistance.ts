import Countdown from "./Countdown";
import Player from "../Indios/Player";

const {ccclass, property} = cc._decorator;

@ccclass
export default class CountDistance extends cc.Component {

    current_Distance = 0.0;
    public static finaldistance = 0;

    @property({
        type: cc.Float
    })
    indio_Speed = 0.1;

    actual_indio_Speed = 0;

    label: cc.Label;

    onLoad () {
        this.label = this.node.getComponent(cc.Label);
        CountDistance.finaldistance = 0;
        this.label.string = CountDistance.finaldistance.toString();
    }

    //start () {}

    update (dt) {
        if(Countdown.Start){
            this.actual_indio_Speed = this.indio_Speed * Player.indioAnim.anim.timeScale;

            this.current_Distance += this.indio_Speed;
    
            if(this.current_Distance >= CountDistance.finaldistance + 1){
                CountDistance.finaldistance += 1;
                this.label.string = CountDistance.finaldistance.toString();
            }
        }
    }
}
