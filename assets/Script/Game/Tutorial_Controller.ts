import BallController from "../Ball/BallController";
import Ball, { Colors } from "../Ball/Ball";
import GameController from "./GameController";
import IndioAnim from "../Indios/IndioAnim";
import Icon from "../Ball/Icon";

const {ccclass, property} = cc._decorator;

export enum Steps {
    first,
    second,
    end,
}

@ccclass
export default class Tutorial_Controller extends cc.Component {

    step: Steps = Steps.end;
    current_step: Steps = Steps.end;

    @property(cc.Node)
    skip: cc.Node = null;

    @property(cc.Node)
    BG: cc.Node = null;

    @property(cc.Node)
    icon: cc.Node = null;

    @property(cc.Node)
    icon_Click: cc.Node = null;

    @property(IndioAnim)
    indio: IndioAnim = null;

    @property(cc.Vec2)
    final_Indio_Pos: cc.Vec2 = null;
    first_Indio_Pos: cc.Vec2 = null;

    @property(cc.Vec2)
    final_Indio_Scale: cc.Vec2 = null;
    first_Indio_Scale: number;

    first_Indio_Anim;

    @property([cc.AudioClip])
    clips: Array<cc.AudioClip> = [];

    @property([cc.String])
    texts: Array<string> = [];

    @property(cc.Label)
    text: cc.Label;

    ballController: BallController;
    gameController: GameController;

    ball: Ball;

    source: cc.AudioSource;

    decrease = false;
    clicked = false;
    green = true;

    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        this.skip.active = false;
        this.BG.active = false;
        this.source = this.node.getComponent(cc.AudioSource);

        this.icon.active = false;
        this.icon_Click.active = false;
    }

    //start () {}

    Start_Tutorial(){
        this.skip.active = true;
        this.BG.active = true;
        this.step = Steps.first;

        this.ballController = this.node.getComponent(BallController);
        this.gameController = this.node.getComponent(GameController);

        this.first_Indio_Pos = this.indio.node.position;
        this.first_Indio_Scale = this.indio.node.scale;

        this.indio.node.position = this.final_Indio_Pos;

        this.indio.node.scaleX = this.final_Indio_Scale.x;
        this.indio.node.scaleY = this.final_Indio_Scale.y;

        this.first_Indio_Anim = this.indio.anim.animationName;
        this.indio.anim.playAnimation(this.indio.name_Anim, -1);
        
        this.Check_Step();
    }

    Check_Step(){
        switch(this.step){
            case Steps.first: {
                this.Clean_Ball();
                this.Spawn_Ball();
                this.Play_Audio(0);
                this.Active_Desactive(false);
                this.text.string = this.texts[0];
                this.step = Steps.second;
                this.current_step = Steps.first;
                break;
            }
            case Steps.second: {
                this.green != this.green;
                this.Clean_Ball();
                this.Spawn_Ball();
                this.Play_Audio(1);
                this.Active_Desactive(false);
                this.text.string = this.texts[1];
                this.step = Steps.end;
                this.current_step = Steps.second;
                break;
            }
            default: {
                this.Skip();
                break;
            }
        }
    }

    Skip(){
        this.source.stop();

        this.unscheduleAllCallbacks();
        this.gameController.stopWatch.active = true;
        this.skip.active = false;
        this.BG.active = false;
        this.step = Steps.end;
        this.Clean_Ball();

        this.indio.node.position = this.first_Indio_Pos;
        this.indio.node.scale = this.first_Indio_Scale;

        this.indio.anim.playAnimation(this.first_Indio_Anim, -1);
        this.indio.anim.timeScale = 1;
    }

    Play_Audio(i){
        this.source.stop();
        this.source.clip = this.clips[i];
        this.source.play();
    }

    Clean_Ball(){
        if(this.ball != null){
            this.ball.node.destroy();
            this.gameController.icon.Hit_FeedBack();
            this.ball = null;
        }
    }

    Active_Desactive(Click){
        if(Click){
            this.icon.active = false;
            this.icon_Click.active = true;
        }else{
            this.icon.active = true;
            this.icon_Click.active = false;
        }
    }

    Spawn_Ball(){
        if(this.green){
            this.ball = cc.instantiate(this.ballController.greenBall).getComponent(Ball);
        }else{
            this.ball = cc.instantiate(this.ballController.yellowBall).getComponent(Ball);
        }

        this.ball.node.setParent(this.ballController.icon);
        this.ball.node.position = new cc.Vec2(this.ballController.Ini_BallPos, 0);
    }

    update (dt) {
        if (this.ball != null){
            if(this.ball.node != null && this.current_step == Steps.first && this.ball.node.position.x < 100){
                
                this.ball.FeedBack();
                this.gameController.icon.Hit_FeedBack();
                this.ball = null;

                this.Spawn_Ball();
    
                this.indio.Increase_Speed();

                this.Active_Desactive(true);
                
                this.schedule(function(){    
                    this.Active_Desactive(false);
                },0.2,0);
            }
    
            if(this.current_step == Steps.second){
                if (this.ball.node == null){
                    this.Spawn_Ball();
                    this.decrease = false;
                    this.clicked = false;
                }
                else if(this.ball.node != null){
                    if(this.ball.type_Ball == Colors.Green){
                        if (!this.decrease && this.ball.node != null && this.ball.node.position.x  < Ball.uncheck_BallPos){
                            this.indio.Decrease_Speed();
                            this.decrease = true;
                        }
            
                        if(!this.clicked && this.ball.node != null && this.ball.node.position.x < 670){
                            this.Active_Desactive(true);
                            
                            this.schedule(function(){    
                                this.Active_Desactive(false);
                            },0.2,0);
                            this.clicked = true;
                        }
                    }
                    else if(this.ball.node.position.x < 100){

                        this.ball.FeedBack();
                        this.gameController.icon.Hit_FeedBack();
                        this.ball = null;
        
                        this.Spawn_Ball();
            
                        this.indio.Decrease_Speed();
        
                        this.Active_Desactive(true);
                        
                        this.schedule(function(){    
                            this.Active_Desactive(false);
                        },0.2,0);
                    }
                }
            }
        }
    }
}
