const {ccclass, property} = cc._decorator;

@ccclass
export default class Countdown extends cc.Component {

    label: cc.RichText = null;

    count = 3;

    source: cc.AudioSource = null;

    @property(cc.AudioClip)
    countdown_Clip: cc.AudioClip = null;

    public static Start = false;

    start () {
        this.source = this.node.getComponent(cc.AudioSource);
        this.label = this.node.getComponent(cc.RichText);

        this.source.clip = this.countdown_Clip;
        this.source.play();

        this.label.string = this.count.toString();

        this.schedule(function() {
            this.count --;
            if(this.count > 0){
                this.label.string = this.count.toString();
            }else{
                this.label.node.active = false;
                Countdown.Start = true;
            }
        }, 1, 3);
    }
}
