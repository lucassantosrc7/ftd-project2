import Countdown from "./Countdown";
import Player from "../Indios/Player";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MoveScenario extends cc.Component {

    speed = 0;

    @property({
        type: Number
    })
    ini_Speed = 1.5;

    @property({
        type: Number
    })
    max_Speed = 1.5;

    @property({
        type: cc.Float
    })
    FinalPos_X = 0.0;

    @property({
        type: cc.Float
    })
    InicialMove_X = 0.0;

    @property(Boolean)
    test: Boolean = false;
    
    update (dt) {
        if(Countdown.Start){
            if(this.test){
                this.speed = this.ini_Speed;
            }else{
                this.speed = (this.max_Speed - this.ini_Speed) * Player.indioAnim.anim.timeScale;
            }
    
            if(this.node.position.x > this.FinalPos_X){
                this.node.position = new cc.Vec2(this.node.position.x - this.speed * dt, this.node.position.y);
            }else if (this.node.position.x <= this.FinalPos_X){
                this.node.position = new cc.Vec2(this.InicialMove_X, this.node.position.y);
            }
        }
    }

    //onLoad () {}
}
